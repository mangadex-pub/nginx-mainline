#!/usr/bin/env bash

set -euo pipefail

if [ -n "${NGINX_BINARY:-}" ]; then
  echo "nginx binary overriden: $NGINX_BINARY"
else
  NGINX_BINARY="/usr/sbin/nginx"
fi

if ! command -v "$NGINX_BINARY" >/dev/null; then
  echo "!! Could not find nginx binary '$NGINX_BINARY'"
  exit 1
fi

NGINX_VERSION_INFO="$("$NGINX_BINARY" -v 2>&1 | cut -d '/' -f2)"
echo "Starting $NGINX_BINARY $NGINX_VERSION_INFO / build: ${IMAGE_VERSION:-local-SNAPSHOT}"

NGINX_CONF_FILE=${1:-${NGINX_CONF_FILE:-"/opt/mangadex/nginx.conf"}}
if ! [ -f "$NGINX_CONF_FILE" ]; then
  echo "Expected nginx configuration file at $NGINX_CONF_FILE, but it didn't exist."
  exit 1
fi
echo "Using configuration file: $NGINX_CONF_FILE"

NGINX_TMP_DIR=${2:-${NGINX_TMP_DIR:-"/tmp/nginx-tmp"}}
echo "Using nginx temporary directory: $NGINX_TMP_DIR"
if ! [ -d "$NGINX_TMP_DIR" ]; then
  echo "Temporary directory doesn't exist. Creating it."
  mkdir -pv "$NGINX_TMP_DIR"
fi

exec "${NGINX_BINARY}" -c "$NGINX_CONF_FILE"
