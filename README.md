# nginx mainline

## Summary

This is a mainline nginx base docker image. Its main goals are:

- Mainline (and unaltered) version of nginx from official repositories, without extra modules
- No default nginx configuration is preseved in the image to avoid loading it by mistake
- Simple support for non-root entrypoint, nginx master process as pid 1, and immutable root filesystem

It is also voluntarily not alpine-based due to various issues with the likes of DNS configuration still being
inconsistent with those of libc to this day and the size savings being minor at best.

The [examples](./example) directory features an example nginx configuration file for use with it.

## Get started

```bash
docker run -it --rm \
  -p "8080:8080" \
  -v "/path/to/mysite/nginx.conf:/opt/mangadex/nginx.conf:ro" \
  -v "/path/to/mysite/webroot:/var/www/mangadex:ro" \
  --tmpfs "/tmp/nginx-tmp" \
  --read-only \
  registry.gitlab.com/mangadex-pub/nginx-mainline:bookworm
```

This command is based on the [examples](example) directory. Note the following:

- If the root filesystem is immutable, the tmpfs mount must be exactly "$NGINX_TMP_DIR" or a parent folder of it. It can
  also be a regular volume.
- If the root filesystem is immutable, the location of "$NGINX_TMP_DIR" must match the temporary paths in nginx.conf
  (see the [example nginx.conf](example/nginx.conf))
- With the docker cli, volume mounts typically must be absolute paths, hence the `$(pwd)` in the example

Refer to the docker and/or nginx docs for more.

## Variables

| Environment variable | Default value            | Description                                                                                            |
|----------------------|--------------------------|--------------------------------------------------------------------------------------------------------|
| NGINX_CONF_FILE      | /opt/mangadex/nginx.conf | Path to nginx.conf. **Must** be an absolute path within the image. Must exist.                         |
| NGINX_TMP_DIR        | /tmp/nginx-tmp           | Temporary directory for nginx buffers. Relative to workdir or absolute. Recursively created if absent. |

*: If the temporary directory is on a tmpfs, make sure to mount it with the `exec` attribute.

Alternatively, when building an image based on this one, these values can be passed as positional arguments like so:

```bash
/usr/local/bin/start-nginx.sh $NGINX_CONF_FILE $NGINX_TMP_DIR
```

The lookup of their runtime value happens in this order, stopping at the first match:

1. Positional argument
2. Environment variable
3. Default value

## Versioning

Builds are prescheduled to run weekly. Multiple tags are published on every build as follows:

- A unique build tag: `$DEBIAN_CODENAME-git-$SHA8-build-$YYYYmmDD-HHMM`
  - The timestamp here is the **build job** start time (as
    per [CI_JOB_STARTED_AT](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html))
  - eg: `buster-git-cf889c55-build-20220405-1235`
- A rolling git commit tag: `git-$SHA8`
  - eg: `git-cf889c55`
- A rolling debian version tag: `$DEBIAN_CODENAME`
  - eg: `bookworm`
- A rolling git reference (see [CI_COMMIT_REF_SLUG](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html))
  tag:
  - eg: `main`

This allows a certain level of flexibility depending on how precise you want to be downstream of this image.

In general we only offer moral support however, so do not expect anything besides bugfixes maybe. You are free to fork
this as you see fit.

## FAQ

### debug logging doesn't work

To use debug logging, you need both to enable them and
to [use an nginx binary compiled with `--with-debug`](https://nginx.org/en/docs/debugging_log.html).

First, enable debug logs:

```nginx
error_log stderr debug;
```

Then set the following environment variable:

```shell
export NGINX_BINARY="/usr/sbin/nginx-debug"
```

### nginx "could not open error log file"

nginx raises the following error on startup if using an immutable root filesystem:

    nginx: [alert] could not open error log file: open() "/var/log/nginx/error.log" failed (2: No such file or directory)"

It can safely be ignored.

Errors log to standard out but nginx looks for this file no matter what, based on compilation arguments. Even if
your `error_log` directive is at the root of `nginx.conf`.

This is because nginx initializes its logging subsystem before reading your nginx.conf file.
