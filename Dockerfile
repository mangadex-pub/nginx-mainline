ARG DEBIAN_CODENAME="bookworm"
FROM docker.io/library/debian:${DEBIAN_CODENAME}-slim

LABEL maintainer="MangaDex open-source <opensource@mangadex.org>"

ARG IMAGE_VERSION
ENV IMAGE_VERSION="$IMAGE_VERSION"
ARG IMAGE_VERSION
LABEL version="$IMAGE_VERSION"

ARG DEBIAN_CODENAME
ENV DEBIAN_CODENAME "$DEBIAN_CODENAME"

ENV DEBIAN_FRONTEND "noninteractive"
ENV TZ "UTC"
RUN echo 'Dpkg::Progress-Fancy "0";' > /etc/apt/apt.conf.d/99progressbar

RUN apt -q update && \
    apt -qq -y full-upgrade && \
    apt -qq -y autoremove && \
    apt -qq -y --no-install-recommends install \
      apt-transport-https \
      apt-utils \
      ca-certificates \
      curl \
      gnupg2 \
      debian-archive-keyring

RUN curl -Ss https://nginx.org/keys/nginx_signing.key | gpg --dearmor | tee /usr/share/keyrings/nginx-archive-keyring.gpg >/dev/null && \
    gpg --dry-run --quiet --import --import-options import-show /usr/share/keyrings/nginx-archive-keyring.gpg | grep "573BFD6B3D8FBC641079A6ABABF5BD827BD9BF62" && \
    echo "deb [signed-by=/usr/share/keyrings/nginx-archive-keyring.gpg] https://nginx.org/packages/mainline/debian ${DEBIAN_CODENAME} nginx" | tee /etc/apt/sources.list.d/nginx.list && \
    echo "Package: nginx\nPin: origin nginx.org\nPin: release o=nginx\nPin-Priority: 900\n" | tee /etc/apt/preferences.d/99nginx

RUN apt -q update && \
    apt -qq -y --no-install-recommends install nginx && \
    apt -qq -y --purge autoremove && \
    apt -qq -y clean && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* /var/cache/* /var/log/*

# Ensure default nginx configuration is wiped, only preserving mime types file
RUN cp -fv /etc/nginx/mime.types /mime.types.orig && \
    rm -rf /etc/nginx && \
    mkdir -m=0755 -v /etc/nginx && \
    mv -fv /mime.types.orig /etc/nginx/mime.types && \
    chmod 0644 /etc/nginx/mime.types

RUN groupadd -r -g 9999 mangadex && \
    useradd -u 9999 -r -g 9999 mangadex

USER mangadex
WORKDIR /tmp

COPY start-nginx.sh /usr/local/bin/start-nginx.sh

CMD ["/usr/local/bin/start-nginx.sh"]
